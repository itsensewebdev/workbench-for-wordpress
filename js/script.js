console.log('Start WorkBench for Wordpress');
var inaw;
var idir;
var isz;

var kinstal_od;
var kinstal_do;

var moc_instalacji;
var rachunek;

iskosny_od = Array;

iskosny_od = [15920,
    16490,
    18564,
    20629,
    21030,
    22956,
    25153,
    26351,
    27064,
    29092,
    31089,
    32745,
    33185,
    35585,
    37842,
    38952,
    40147,
    42425,
    44676,
    45456,
    46899,
    49096,
    50241,
    51336,
    53571,
    55834,
    56147,
    57936,
    60010,
    62056,
    63411,
    64260,
    66449,
    68605,
    69204
];


iskosny_do = Array;

iskosny_do = [22451,
    23058,
    23574,
    26460,
    27451,
    28326,
    31799,
    32444,
    33265,
    35230,
    37576,
    38545,
    39541,
    42011,
    43594,
    45020,
    46065,
    48910,
    50685,
    52123,
    54354,
    56195,
    57002,
    57905,
    60348,
    62904,
    64021,
    65524,
    68747,
    70189,
    70987,
    71971,
    73684,
    75527,
    77316
];




function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
    try {
        decimalCount = Math.abs(decimalCount);
        decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

        const negativeSign = amount < 0 ? "-" : "";

        let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
        let j = (i.length > 3) ? i.length % 3 : 0;

        return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
    } catch (e) {
        console.log(e)
    }
};





jQuery('#slider').val(0);



jQuery(document).on('input', '#slider', function() {

    jQuery('#slider_value').html(jQuery(this).val() + ' zł');

    rachunek = jQuery(this).val() + ' zł';

    rachunek_v = jQuery(this).val();

    mi_zuz_pr = jQuery(this).val() / 0.63;


    moc_instalacji = (mi_zuz_pr * 12) / 1000 * 1.15;



    if (moc_instalacji < 3) {
        jQuery('.step2').fadeIn();
        jQuery('.step2').html('<b>Instalacja fotowoltaiczna nie bedzie opłacalna</b>');
    }

    if (moc_instalacji > 3) {
        jQuery('.step2').fadeIn();
        jQuery('.step2').html('<button type="button" class="arrow nochover_arrow" style="background-color: #fff; "><img src="https://ultraenergy.pl/wp-content/plugins/workbench-for-wordpress//images/arrow.png">     <p>Przejdź dalej</p></button>');
    }


    if (moc_instalacji > 3) {

        if ((moc_instalacji > 3) && (moc_instalacji <= 3.5)) prefix_cennik = 0;
        if ((moc_instalacji > 3.5) && (moc_instalacji <= 4)) prefix_cennik = 1;
        if ((moc_instalacji > 4) && (moc_instalacji <= 4.5)) prefix_cennik = 2;
        if ((moc_instalacji > 4.5) && (moc_instalacji <= 5)) prefix_cennik = 3;
        if ((moc_instalacji > 5) && (moc_instalacji <= 5.5)) prefix_cennik = 4;
        if ((moc_instalacji > 5.5) && (moc_instalacji <= 6)) prefix_cennik = 5;
        if ((moc_instalacji > 6) && (moc_instalacji <= 6.5)) prefix_cennik = 6;
        if ((moc_instalacji > 6.5) && (moc_instalacji <= 7)) prefix_cennik = 7;
        if ((moc_instalacji > 7) && (moc_instalacji <= 7.5)) prefix_cennik = 8;
        if ((moc_instalacji > 7.5) && (moc_instalacji <= 8)) prefix_cennik = 9;
        if ((moc_instalacji > 8) && (moc_instalacji <= 8.5)) prefix_cennik = 10;
        if ((moc_instalacji > 8.5) && (moc_instalacji <= 9)) prefix_cennik = 11;
        if ((moc_instalacji > 9) && (moc_instalacji <= 9.5)) prefix_cennik = 12;
        if ((moc_instalacji > 9.5) && (moc_instalacji <= 10)) prefix_cennik = 13;
        if ((moc_instalacji > 10) && (moc_instalacji <= 10.5)) prefix_cennik = 14;
        if ((moc_instalacji > 10.5) && (moc_instalacji <= 11)) prefix_cennik = 15;
        if ((moc_instalacji > 11) && (moc_instalacji <= 11.5)) prefix_cennik = 16;
        if ((moc_instalacji > 11.5) && (moc_instalacji <= 12)) prefix_cennik = 17;
        if ((moc_instalacji > 12) && (moc_instalacji <= 12.5)) prefix_cennik = 18;
        if ((moc_instalacji > 12.5) && (moc_instalacji <= 13)) prefix_cennik = 19;
        if ((moc_instalacji > 13) && (moc_instalacji <= 13.5)) prefix_cennik = 20;
        if ((moc_instalacji > 13.5) && (moc_instalacji <= 14)) prefix_cennik = 21;
        if ((moc_instalacji > 14) && (moc_instalacji <= 14.5)) prefix_cennik = 22;
        if ((moc_instalacji > 14.5) && (moc_instalacji <= 15)) prefix_cennik = 23;
        if ((moc_instalacji > 15) && (moc_instalacji <= 15.5)) prefix_cennik = 24;
        if ((moc_instalacji > 15.5) && (moc_instalacji <= 16)) prefix_cennik = 25;
        if ((moc_instalacji > 16) && (moc_instalacji <= 16.5)) prefix_cennik = 26;
        if ((moc_instalacji > 16.5) && (moc_instalacji <= 17)) prefix_cennik = 27;
        if ((moc_instalacji > 17) && (moc_instalacji <= 17.5)) prefix_cennik = 28;
        if ((moc_instalacji > 17.5) && (moc_instalacji <= 18)) prefix_cennik = 29;
        if ((moc_instalacji > 18) && (moc_instalacji <= 18.5)) prefix_cennik = 30;
        if ((moc_instalacji > 18.5) && (moc_instalacji <= 19)) prefix_cennik = 31;
        if ((moc_instalacji > 19) && (moc_instalacji <= 19.5)) prefix_cennik = 32;
        if ((moc_instalacji > 19.5) && (moc_instalacji <= 20)) prefix_cennik = 33;




        oblicz_instalacje();

    }


});


///////////////////////////////////////////////////////////////////////////////////////

function oblicz_instalacje(inaw, idir, isz) {

    if (moc_instalacji > 3)

    //jQuery('#results').html(Math.round(moc_instalacji));

        imoc = Math.round(moc_instalacji);


    kinstal_od = iskosny_od[prefix_cennik];
    kinstal_do = iskosny_do[prefix_cennik];


    if (inaw == '2') {
        kinstal_od = iskosny_od[prefix_cennik];
        kinstal_do = iskosny_do[prefix_cennik];
    }

    if (inaw == '1') {
        kinstal_od = Math.round(iskosny_od[prefix_cennik] + 200 * moc_instalacji);
        kinstal_do = Math.round(iskosny_do[prefix_cennik] + 200 * moc_instalacji);
    }

    if (inaw == '3') {
        kinstal_od = Math.round(iskosny_od[prefix_cennik] + 400 * moc_instalacji);
        kinstal_do = Math.round(iskosny_do[prefix_cennik] + 400 * moc_instalacji);
    }


    if (idir == '2') {
        kinstal_od = Math.round(kinstal_od + 200 * moc_instalacji);
        kinstal_do = Math.round(kinstal_do + 200 * moc_instalacji);
    }



    if (isz == '2') {
        kinstal_od = Math.round(kinstal_od + 400 * moc_instalacji);
        kinstal_do = Math.round(kinstal_do + 400 * moc_instalacji);
    }

    if (isz == '3') {
        kinstal_od = Math.round(kinstal_od + 800 * moc_instalacji);
        kinstal_do = Math.round(kinstal_do + 800 * moc_instalacji);
    }

    jQuery('#results').html('Koszt Twojej instalacji waha się pomiędzy <b>' + formatMoney(kinstal_od) + '</b> zł a ' + formatMoney(kinstal_do) + ' zł');











    //////////////////////////

    //zaokrąglanie liczb
    function roundPrecised(number, precision) {
        let power = Math.pow(10, precision);

        return Math.round(number * power) / power;
    }




    kinstal_od = kinstal_od + ((kinstal_do - kinstal_od) / 2);


    //$('#dofins_rzad305').html(roundPrecised(0.305 * kinstal_od, 2));



    //tutaj sprawdzam czy należy sie dofinansowanie. Nalezy się gdy 8% VAT oraz moc < 10 kpw

    var dfz = 0;
    dfz = 5000;

    //alert(kinstal_od);

    let kipuu = kinstal_od - eval(roundPrecised(0.17 * kinstal_od, 2)) - dfz;


    // alert(rachunek_v);

    let srednie_ro = rachunek_v * 12;


    let podwyzka15 = srednie_ro * 0.08;
    for (i = 1; i < 16; i++) {
        podwyzka15 = podwyzka15 + podwyzka15 * 0.08;
    }

    let podwyzka30 = srednie_ro * 0.08;
    for (i = 1; i < 31; i++) {
        podwyzka30 = podwyzka30 + podwyzka30 * 0.08;
    }



    let lat15zaprad = roundPrecised(srednie_ro * 15, 2) + podwyzka15;
    let lat30zaprad = roundPrecised(srednie_ro * 30, 2) + podwyzka30;

    let zwrot_po_ulgach = roundPrecised(roundPrecised(kipuu, 2) / srednie_ro, 1);

    jQuery('#zwrot_po_ulgach').html(zwrot_po_ulgach + ' LAT');


    // let lat15zaprad = roundPrecised(srednie_ro*15,2);


    jQuery('#15latzaprad').html(accounting.formatMoney(lat15zaprad, {
        symbol: "zł",
        format: "%v %s"
    }));


    //let lat30zaprad = roundPrecised(srednie_ro*30,2);

    jQuery('#30latzaprad').html(accounting.formatMoney(lat30zaprad, {
        symbol: "zł",
        format: "%v %s"
    }));



    ///////////////////////////


}



jQuery(".roofAngle").hover(
    function() {
        jQuery(this).removeClass('nochover');
        jQuery(this).addClass('activeroof');

    },
    function() {


        if (!jQuery(this).hasClass('clickactiveroof')) {
            jQuery(this).removeClass('activeroof');
            jQuery(this).addClass('nochover');
        }
    }
);


jQuery(".roofAngle").click(
    function() {
        jQuery(".roofAngle").removeClass('clickactiveroof');
        jQuery(".roofAngle").addClass('nochover');
        jQuery(this).removeClass('nochover');
        jQuery(this).addClass('clickactiveroof');

        inaw = jQuery(this).attr('data-idach');
        oblicz_instalacje(inaw);

        jQuery("#step3content").fadeIn();

        jQuery([document.documentElement, document.body]).animate({
            scrollTop: jQuery("#step3content").offset().top - 40
        }, 1000);


    }
);

///////////////////////////////////////////////////////////////////////////////////////

jQuery(".roofDir").hover(
    function() {
        jQuery(this).removeClass('nochover');
        jQuery(this).addClass('activeroof');

    },
    function() {


        if (!jQuery(this).hasClass('clickactiveroof')) {
            jQuery(this).removeClass('activeroof');
            jQuery(this).addClass('nochover');
        }
    }
);

jQuery(".roofDir").click(
    function() {
        jQuery(".roofDir").removeClass('clickactiveroof');
        jQuery(".roofDir").addClass('nochover');
        jQuery(this).removeClass('nochover');
        jQuery(this).addClass('clickactiveroof');


        idir = jQuery(this).attr("data-idir");

        oblicz_instalacje(inaw, idir);

        jQuery("#step4content").fadeIn();

        jQuery([document.documentElement, document.body]).animate({
            scrollTop: jQuery("#step4content").offset().top - 40
        }, 1000);


    }
);


jQuery(".roofDir_NO").hover(
    function() {
        jQuery(this).removeClass('nochover');
        jQuery(this).addClass('activeroof_NO');

        jQuery('#message_polnoc').html('<b>Instalacja nieefektywna</b>');
    },
    function() {


        if (!jQuery(this).hasClass('clickactiveroof')) {
            jQuery(this).removeClass('activeroof_NO');
            jQuery(this).addClass('nochover');
            jQuery('#message_polnoc').html('Północ');
        }
    }
);


/////////////////////////////////////////////////////////////////////////////////////

 

jQuery(".roofSha").hover(
    function() {
        jQuery(this).removeClass('nochover');
        jQuery(this).addClass('activeroof');

    },
    function() {


        if (!jQuery(this).hasClass('clickactiveroof')) {
            jQuery(this).removeClass('activeroof');
            jQuery(this).addClass('nochover');
        }
    }
);



jQuery(".roofSha").click(
    function() {
        jQuery(".roofSha").removeClass('clickactiveroof');
        jQuery(".roofSha").addClass('nochover');
        jQuery(this).removeClass('nochover');
        jQuery(this).addClass('clickactiveroof');

        isz = jQuery(this).attr("data-isz");



        oblicz_instalacje(inaw, idir, isz);


        jQuery("#step5content").fadeIn();
        jQuery([document.documentElement, document.body]).animate({
            scrollTop: jQuery("#step5content").offset().top - 40
        }, 1000);

    }
);





////////////////////////////////////////////////////////////////////////////////




jQuery(".step2").click(
    function() {
        jQuery("#step2content").fadeIn();

        jQuery([document.documentElement, document.body]).animate({
            scrollTop: jQuery("#step2content").offset().top - 40
        }, 1000);

    }
);











jQuery(".arrow").hover(
    function() {

        jQuery(this).removeClass('nochover_arrow');
        jQuery(this).addClass('activeroof_arrow');

    },
    function() {

        jQuery(this).removeClass('activeroof_arrow');
        jQuery(this).addClass('nochover_arrow');

    }
);




jQuery('#sendo').on('submit', function(event) {
    event.preventDefault();

    // Add text 'loading...' right after clicking on the submit button. 
    jQuery('.output_message').text('Wysyłanie...');

    var form = jQuery(this);
    var dta = form.serialize() + "&inaw=" + inaw + "&idir=" + idir + "&isz=" + isz + "&cenaod=" + kinstal_od + "&cenado=" + kinstal_do + "&moc=" + moc_instalacji + "&rachunek=" + rachunek;


    jQuery.ajax({
        url: "/wp-content/plugins/workbench-for-wordpress/mail.php",
        type: 'post',
        data: dta,
        success: function(result) {
            if (result == 'success') {
                jQuery('.output_message').text('Dziękujemy! Twoja widadomość została wysłana!');
                jQuery('#email').val('');
                jQuery('#telefon').val('');
            } else {
                jQuery('.output_message').text('Error!');
            }
        }
    });

    // Prevents default submission of the form after clicking on the submit button. 
    return false;
});
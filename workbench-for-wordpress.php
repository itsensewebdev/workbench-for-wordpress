
<?
/**
 * Plugin Name: WorkBench for Wordpress
 * Plugin URI: https://itsense.pl/
 * Description: Allows to embed frontend app inside any page.
 * Version: 1.0.2
 * Author: Michał Górecki - IT Sense Web Developers
 * Author URI: https://itsense.pl
 * License: GPL-3.0+
 * License URI: https://www.gnu.org/licenses/gpl-3.0.html
 * Text Domain: workbench-for-wordpress
 * Domain Path: /
 *
 * @package WorkBench for Wordpress
 */






function wpb_adding_scripts() {

   wp_register_style('mstyle', plugins_url('css/style.css', __FILE__));

   wp_enqueue_style('mstyle');



    wp_register_script('my_amazing_script', plugins_url('/js/script.js', __FILE__), array('jquery'),'1.3', true);

    wp_register_script('my_amazing_script2', plugins_url('/js/accounting.min.js', __FILE__));

    wp_enqueue_script('my_amazing_script');
    wp_enqueue_script('my_amazing_script2');


    wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js', array(), null, true);

    wp_enqueue_script('jquery');





    wp_register_script('prefix_bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js');

    wp_enqueue_script('prefix_bootstrap');

    

    wp_register_style('prefix_bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css');

    wp_enqueue_style('prefix_bootstrap');





 

    }

      

    add_action( 'wp_enqueue_scripts', 'wpb_adding_scripts' );  











 // function that runs when shortcode is called

function wpb_demo_shortcode() { 

 

   ?>
 

<div class="row">


</div>

    <div class="row">

        <div class="col-sm-12" style="text-align:center">

            <h2 style="color: #07CA00;font-family: 'Montserrat', Sans-serif;font-size: 32px;font-weight: 400;">W 30 SEKUND SPRAWDZISZ, CZY FOTOWOLTAIKA JEST DLA CIEBIE</h2>

        </div>

    </div>



    <div class="row">

        <div class="col-sm-4"></div>

        <div class="col-sm-4" style="text-align:center">

            <p style="color:#555 !important">Ile płacisz za prąd miesięcznie?</p>

            <div class="slidecontainer">

                <input type="range" min="1" max="1000" value="0" class="slider" id="slider">

            </div>

            <div>





                <h2 id="slider_value" style="color:#555 !important">Jeszcze nic.</h2>



                <div style="margin-top:30px" class="step2" style="display: none;">

                    

                </div>









            </div>













        </div>

    </div>



    <div class="row" id="step2content" style="display: none;">

        <div style="margin-top:40px">




            <div class="col-sm-12" style="text-align:center">

                <h2 style="color: #07CA00;font-family: 'Montserrat', Sans-serif;font-size: 32px;font-weight: 400; margin-bottom:2px">JAKI JEST RODZAJ NAWIERZCHNI?</h2>
                <p style="color:#555 !important">Rodzaj nawierzchni, na której powstanie instalacja wpływa na jej koszt. </p>
            </div>






            <div class="col-sm-4 nochover roofAngle" id="roof1" data-idach="1">
                <div class="widget-single-choice">

                    <img alt="" src="<?php echo plugin_dir_url( __FILE__ ).'/images/1.png'; ?>" />

                    <p class="widget-single-choice__title" style="text-align: center;color:#555 !important">Dach płaski</p>



                </div>
            </div>



            <div class="col-sm-4 nochover roofAngle" id="roof2" data-idach="2">
                <div class="option with-icon">

                    <img alt="" src="<?php echo plugin_dir_url( __FILE__ ).'/images/index1-2-.png'; ?>" />

                    <p class="widget-single-choice__title" style="text-align: center;color:#555 !important">Dach skośny</p>


                </div>
            </div>


            <div class="col-sm-4 nochover roofAngle" id="roof3" data-idach="3">
                <div class="option with-icon">

                    <img alt="" src="<?php echo plugin_dir_url( __FILE__ ).'/images/1-3.png'; ?>" />

                    <p class="widget-single-choice__title" style="text-align: center;color:#555 !important">Grunt</p>



                </div>
            </div>



            <div class="row" style="display:none">
                <div class="col-md-12" style="text-align: center;">
                    <div style="margin-top:30px">

                        <button type="button" class="step3 arrow" style="background-color: #fff;"><img src="<?php echo plugin_dir_url( __FILE__ ).'/images/arrow.png'; ?>"></button>

                    </div>
                </div>

            </div>

        </div>

    </div>








    <div class="row" id="step3content" style="display: none;">

        <div style="margin-top:40px">

            <div class="col-sm-12" style="text-align:center">

                <h2 style="color: #07CA00;font-family: 'Montserrat', Sans-serif;font-size: 32px;font-weight: 400; margin-bottom: 2px;">W KTÓRYM KIERUNKU JEST NACHYLONE<br>MIEJSCE MONTAŻU?</h2>
                <p style="color:#555 !important">Uzysk instalacji zależy od ekspozycji dachu.</p>

            </div>



            <div class="col-sm-4 nochover roofDir_NO" id="direroof1" >
                <div class="widget-single-choice">

                    <img alt="" src="<?php echo plugin_dir_url( __FILE__ ).'/images/index3-1.png'; ?>" />

                    <p class="widget-single-choice__title" style="text-align: center;" id="message_polnoc">Północ</p>



                </div>
            </div>


            <div class="col-sm-4 nochover roofDir" id="direroof2" data-idir="1">

                <div class="option with-icon">

                    <img alt="" src="<?php echo plugin_dir_url( __FILE__ ).'/images/index3-2.png'; ?>" />

                    <p class="widget-single-choice__title" style="text-align: center;">Południe</p>



                </div>
            </div>


 


            <div class="col-sm-4 nochover roofDir" id="direroof3" data-idir="2" >
                <div class="option with-icon">

                    <img alt="" src="<?php echo plugin_dir_url( __FILE__ ).'/images/index3-3.png'; ?>" />

                    <p class="widget-single-choice__title" style="text-align: center;">Wschód/Zachód</p>


                </div>
            </div>


       



            <div class="row" style="display:none">
                <div class="col-md-12" style="text-align: center;">
                    <div style="margin-top:30px">

                        <button type="button" class="step4 arrow" style="background-color: #fff;"><img src="<?php echo plugin_dir_url( __FILE__ ).'/images/arrow.png'; ?>"></button>

                    </div>
                </div>

            </div>

        </div>

    </div>









    <div class="row" id="step4content" style="display: none;">

        <div style="margin-top:40px">

            <div class="col-sm-12" style="text-align:center">

                <h2 style="color: #07CA00;font-family: 'Montserrat', Sans-serif;font-size: 32px;font-weight: 400; margin-bottom: 2px;">ILE CIENIA PADA NA MIEJSCE MONTAŻU?</h2>
                <p>Cień negatywnie wpływa na sprawność instalacji fotowoltaicznej</p>
            </div>



            <div class="col-sm-4 nochover roofSha" id="shroof1" data-isz="1">
                <div class="widget-single-choice">

                    <img alt="" src="<?php echo plugin_dir_url( __FILE__ ).'/images/4-1.png'; ?>" />

                    <p class="widget-single-choice__title" style="text-align: center;">Brak cienia</p>
                    <p class="widget-single-choice__info" style="text-align: center;">Dach całkowicie odsłonięty</p>



                </div>
            </div>


            <div class="col-sm-4 nochover roofSha" id="shroof2" data-isz="2">

                <div class="option with-icon">

                    <img alt="" src="<?php echo plugin_dir_url( __FILE__ ).'/images/4-2.png'; ?>" />

                    <p class="widget-single-choice__title" style="text-align: center;">Trochę cienia</p>
                    <p class="widget-single-choice__info" style="text-align: center;">Dach częściowo zacieniony</p>




                </div>
            </div>

            <div class="col-sm-4 nochover roofSha" id="shroof3" data-isz="3">
                <div class="option with-icon">

                    <img alt="" src="<?php echo plugin_dir_url( __FILE__ ).'/images/4-3.png'; ?>" />

                    <p class="widget-single-choice__title" style="text-align: center;">Dużo cienia</p>

                    <p class="widget-single-choice__info" style="text-align: center;">Dach prawie cały zacieniony</p>

                </div>
            </div>






          

        </div>

    </div>





    <div class="row" id="step5content" style="display: none;">

        <div style="margin-top:80px">

            <div class="col-sm-12" style="text-align:center">

                <div id="results" style="font-size:24px; font-weight:bold"></div>

                <p  style="margin-top: 30px;font-size:22px">Instalacja fotowoltaiczna to realne oszczędności</p>
                




                <div class="panel-body">
                            <div class="row">


                                <div class="col-md-4">
                                
<div class="circle">
  <div class="circle__inner">
    <div class="circle__wrapper">
      <div class="circle__content">ZWROT INWESTYCJI W CIĄGU<br><span id="zwrot_po_ulgach"></span> </div>
    </div>
  </div>
</div>
                                </div>


                                <div class="col-md-4">
                                
                                <div class="circle">
                                  <div class="circle__inner">
                                    <div class="circle__wrapper">
                                      <div class="circle__content">XXX </div>
                                    </div>
                                  </div>
                                </div>
                                                                </div>


                                                                <div class="col-md-4">
                                
                                <div class="circle">
                                  <div class="circle__inner">
                                    <div class="circle__wrapper">
                                      <div class="circle__content">XXX </div>
                                    </div>
                                  </div>
                                </div>
                                                                </div>


                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="table-responsive">
                                        <table class="table">
                                            
                                            <tbody>
                                               


                                                <tr>
                                               
                                                    <td>Zwrot po ulgach i podwyżkach</td>
                                                    <td id="2zwrot_po_ulgach" style="text-align:right"></td>


                                                </tr>


                                                <tr>
                                                  
                                                    <td>15 lat za prąd (uwzg. podwyżki 8% rocznie)</td>
                                                    <td id="15latzaprad" style="text-align:right"></td>


                                                </tr>

                                                <tr>
                                                
                                                    <td>30 lat za prąd (uwzg. podwyżki 8% rocznie)</td>
                                                    <td id="30latzaprad" style="text-align:right"></td>


                                                </tr>


                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>








                <p id="results_message" style="margin-top: 20px;"></p>
                
                <p id="" style="margin-top: 20px;font-size:20px">Zostaw swój email i telefon. Skontaktujemy się z Tobą.</p>

                <form type="" method="POST" name="sendo" id="sendo">
                <p><input type="text" name="name" id="name" placeholder="Imię i nazwisko" required></p>
                    <p><input type="email" name="email" id="email" placeholder="Twój email" required></p>
                    <p><input type="text" name="telefon" id="telefon" placeholder="Twój telefon" required></p>
                  
            
                    <p><input type="checkbox" name="rodo" id="rodo" required> Wyrażam zgodę na przetwarzanie moich danych osobowych podanych w powyższym formularzu. Ponadto wyrażam chęć kontaktu ze strony firmy Ultra Energy Sp. z o.o. w celu przedstawienia mi oferty handlowej oraz materiałów marketingowych.</p>
                    <p><button type="submit">Wyślij</button></p>
                    </form>
                    <div class="output_message"> </div>
            </div>

        </div>

    </div>


    <?php  

   

     

    // Output needs to be return

    return $message;

    } 

    // register shortcode

    add_shortcode('workbench', 'wpb_demo_shortcode'); 







?>